# chipbox-photos

Some photos of Chipbox satellite receiver

## Front

![front-top](front-top-overall.jpg)

![DRAMS](nanya-dram.jpg)

![NOR Flash](front-top-nor-flash.jpg)

![RTL8201CP](rtl8201cp.jpg)

![CAT6612CQ](cat6612cq.jpg)

![front-top](front-top-1.jpg)

![left-top](left-top-1.jpg)

![left-top-dram](left-top-dram-clearly-visible.jpg)

![REAR VIEW](rear-view.jpg)
